// Keep the blockSize reasonable because speed and fire size don't adapt to it
static int blockSize = 30;
static int mapX = 50;
static int mapY = 25;

static World world;

void settings() {
  size(mapX*blockSize, mapY*blockSize);
  //fullScreen();
}

void setup() {
  world = new World(mapX, mapY);
}

void draw() {
  background(255, 255, 255);
  world.update();
  world.draw();
}

// ######################################################## BLOCK ########################################################

abstract class Block {
  abstract public boolean update();
  abstract public void draw();
  abstract public boolean collide();
  abstract public String getName();
  abstract public void fire();
  abstract public void water();
}

class Grass extends Block {
  private String name = "Grass";
  private int x;
  private int y;
  private boolean collide = false;
  private boolean fancy = false; // VERY LAGGY
  private int nb = 7; // blockSize for full detail
  
  public Grass(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public boolean update() {
    return true;
  }
  public void draw() {
    rectMode(CORNER);
    resetMatrix();
    noStroke();
    
    if(this.fancy) {
      colorMode(HSB, 100);
      for(int i = 0; i <= blockSize; i+=blockSize/this.nb) {
        if(i < blockSize/2) {
          fill(30, 100, 100-(i/(float)blockSize*50));
        }
        else {
          fill(30, 100, 50+(i/(float)blockSize*50));
        }
        
        rect(this.x*blockSize + i, this.y*blockSize, this.x*blockSize + i /* + blockSize/this.nb*/, this.y*blockSize + blockSize);
      }
      colorMode(RGB, 255);
    }
    else {
      fill(50, 200, 50);
      stroke(0, 200, 0);
      rect(this.x*blockSize, this.y*blockSize, this.x*blockSize + blockSize, this.y*blockSize + blockSize);
    }
  }
  public boolean collide() {
    return this.collide;
  }
  public String getName() {
    return this.name;
  }
  public void fire() {}
  public void water() {}
}
class Wall extends Block {
  private String name = "Wall";
  private int x;
  private int y;
  private boolean collide = true;
  private boolean fancy = true;
  
  public Wall(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public boolean update() {
    return true;
  }
  public void draw() {
    rectMode(CORNER);
    resetMatrix();
    if(this.fancy) {
      fill(200, 100, 0);
      stroke(128, 128, 128);
      rect(this.x*blockSize, this.y*blockSize, blockSize/2, blockSize/4);
      rect(this.x*blockSize + blockSize/2, this.y*blockSize, blockSize/2, blockSize/4);
      
      rect(this.x*blockSize, this.y*blockSize + blockSize/4, blockSize/3, blockSize/4);
      rect(this.x*blockSize + blockSize*1/3, this.y*blockSize + blockSize/4, blockSize/3, blockSize/4);
      rect(this.x*blockSize + blockSize*2/3, this.y*blockSize + blockSize/4, blockSize/3, blockSize/4);
      
      rect(this.x*blockSize, this.y*blockSize + blockSize*2/4, blockSize/2, blockSize/4);
      rect(this.x*blockSize + blockSize/2, this.y*blockSize + blockSize*2/4, blockSize/2, blockSize/4);
      
      rect(this.x*blockSize, this.y*blockSize + blockSize*3/4, blockSize/3, blockSize/4);
      rect(this.x*blockSize + blockSize*1/3, this.y*blockSize + blockSize*3/4, blockSize/3, blockSize/4);
      rect(this.x*blockSize + blockSize*2/3, this.y*blockSize + blockSize*3/4, blockSize/3, blockSize/4);
    }
    else {
      fill(200, 100, 0);
      stroke(128, 128, 128);
      rect(this.x*blockSize, this.y*blockSize, blockSize, blockSize);
      /*fill(0, 0, 0);
      stroke(0, 0, 0);
      text(this.name, x*blockSize+blockSize/4, y*blockSize+blockSize/2);*/
    }
  }
  public boolean collide() {
    return this.collide;
  }
  public String getName() {
    return this.name;
  }
  public void fire() {}
  public void water() {}
}


class Steps extends Block {
  private String name = "Steps";
  private int x;
  private int y;
  private boolean collide = false;
  private boolean fancy = true;
  private Block under;
  
  public Steps(int x, int y) {
    this.x = x;
    this.y = y;
    this.under = new Grass(x, y);
  }
  
  public boolean update() {
    return true;
  }
  public void draw() {
    rectMode(CORNER);
    resetMatrix();
    this.under.draw();
    if(this.fancy || !this.fancy) { // Always same texture
      fill(150, 150, 150);
      stroke(128, 128, 128);
      rect(this.x*blockSize + blockSize*1/8, this.y*blockSize + blockSize*1/8, blockSize/8, blockSize/8);
      rect(this.x*blockSize + blockSize*3/8, this.y*blockSize + blockSize*2/8, blockSize/8, blockSize/8);
      rect(this.x*blockSize + blockSize*6/8, this.y*blockSize + blockSize*3/8, blockSize/8, blockSize/8);
      rect(this.x*blockSize + blockSize*5/8, this.y*blockSize + blockSize*6/8, blockSize/8, blockSize/8);
      rect(this.x*blockSize + blockSize*3/8, this.y*blockSize + blockSize*7/8, blockSize/8, blockSize/8);
      rect(this.x*blockSize + blockSize*2/8, this.y*blockSize + blockSize*5/8, blockSize/8, blockSize/8);
      rect(this.x*blockSize + blockSize*7/8, this.y*blockSize, blockSize/8, blockSize/8);
    }
  }
  public boolean collide() {
    return this.collide;
  }
  public String getName() {
    return this.name;
  }
  public void fire() {}
  public void water() {}
}

class Wood extends Block {
  private String name = "Wood";
  private int x;
  private int y;
  private boolean collide = true;
  private boolean fancy = true;
  private int intensity = 20;
  private boolean onFire = false;
  private int fireTick = 0;
  private int fireLife = 200;
  private ArrayList<Particle> particles = new ArrayList<Particle>();
  
  public Wood(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public boolean update() {
    if(this.onFire) {
      this.fireTick++;
    }
    
    // Fire creation
    if(this.onFire) {
      for(int i = 1; i <= this.intensity; i++) {
        this.particles.add(new Fire(new PVector(this.x*blockSize + blockSize*0.5, this.y*blockSize + blockSize*0.9), new PVector(0, 0), 75, 0.25, 0.25));
      }
    }
    
    // Fire update
    for(int i = 0; i < this.particles.size(); i++) {
      if(!this.particles.get(i).update()) {
        this.particles.remove(i);
      }
    }
    
    return this.fireTick < this.fireLife;
  }
  public void draw() {
    rectMode(CORNER);
    resetMatrix();
    if(this.fancy) {
      fill(200,150,100);
      stroke(125,100,75);
      
      rect(this.x*blockSize, this.y*blockSize, blockSize*2/3, blockSize/5);
      rect(this.x*blockSize + blockSize*2/3, this.y*blockSize, blockSize/3, blockSize/5);
      
      rect(this.x*blockSize, this.y*blockSize + blockSize/5, blockSize/3, blockSize/5);
      rect(this.x*blockSize + blockSize*1/3, this.y*blockSize + blockSize/5, blockSize*2/3, blockSize/5);
      
      
      rect(this.x*blockSize, this.y*blockSize + blockSize*2/5, blockSize/2, blockSize/5);
      rect(this.x*blockSize + blockSize/2, this.y*blockSize + blockSize*2/5, blockSize/2, blockSize/5);
      
      rect(this.x*blockSize, this.y*blockSize + blockSize*3/5, blockSize*2/3, blockSize/5);
      rect(this.x*blockSize + blockSize*2/3, this.y*blockSize + blockSize*3/5, blockSize*1/3, blockSize/5);
      
      rect(this.x*blockSize, this.y*blockSize + blockSize*4/5, blockSize/3, blockSize/5);
      rect(this.x*blockSize + blockSize*1/3, this.y*blockSize + blockSize*4/5, blockSize*2/3, blockSize/5);
      
    }
    else {
      fill(200,150,100);
      stroke(125,100,75);
      rect(this.x*blockSize, this.y*blockSize, blockSize, blockSize);
      /*fill(0, 0, 0);
      stroke(0, 0, 0);
      text(this.name, x*blockSize+blockSize/4, y*blockSize+blockSize/2);*/
    }
    
    // Fire draw
    for(int i = 0; i < this.particles.size(); i++) {
      this.particles.get(i).draw();
    }
    
  }
  public boolean collide() {
    return this.collide;
  }
  public String getName() {
    return this.name;
  }
  public void fire() {
    this.onFire = true;
  }
  public void water() {
    this.onFire = false;
    this.fireTick = 0;
  }
}
class Flooring extends Block {
  private String name = "Flooring";
  private int x;
  private int y;
  private boolean collide = false;
  private boolean fancy = true;
  private int intensity = 20;
  private boolean onFire = false;
  private int fireTick = 0;
  private int fireLife = 200;
  private ArrayList<Particle> particles = new ArrayList<Particle>();
  
  public Flooring(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public boolean update() {
    if(this.onFire) {
      this.fireTick++;
    }
    
    // Fire creation
    if(this.onFire) {
      for(int i = 1; i <= this.intensity; i++) {
        this.particles.add(new Fire(new PVector(this.x*blockSize + blockSize*0.5, this.y*blockSize + blockSize*0.9), new PVector(0, 0), 75, 0.25, 0.25));
      }
    }
    // Fire update
    for(int i = 0; i < this.particles.size(); i++) {
      if(!this.particles.get(i).update()) {
        this.particles.remove(i);
      }
    }
    
    return this.fireTick < this.fireLife;
  }
  public void draw() {
    rectMode(CORNER);
    resetMatrix();
    if(this.fancy) {
      fill(150,100,50);
      stroke(75,50,20);
      
      rect(this.x*blockSize, this.y*blockSize, blockSize*2/3, blockSize/5);
      rect(this.x*blockSize + blockSize*2/3, this.y*blockSize, blockSize/3, blockSize/5);
      
      rect(this.x*blockSize, this.y*blockSize + blockSize/5, blockSize/3, blockSize/5);
      rect(this.x*blockSize + blockSize*1/3, this.y*blockSize + blockSize/5, blockSize*2/3, blockSize/5);
      
      
      rect(this.x*blockSize, this.y*blockSize + blockSize*2/5, blockSize/2, blockSize/5);
      rect(this.x*blockSize + blockSize/2, this.y*blockSize + blockSize*2/5, blockSize/2, blockSize/5);
      
      rect(this.x*blockSize, this.y*blockSize + blockSize*3/5, blockSize*2/3, blockSize/5);
      rect(this.x*blockSize + blockSize*2/3, this.y*blockSize + blockSize*3/5, blockSize*1/3, blockSize/5);
      
      rect(this.x*blockSize, this.y*blockSize + blockSize*4/5, blockSize/3, blockSize/5);
      rect(this.x*blockSize + blockSize*1/3, this.y*blockSize + blockSize*4/5, blockSize*2/3, blockSize/5);
      
    }
    else {
      fill(150,100,50);
      stroke(75,50,20);
      rect(this.x*blockSize, this.y*blockSize, blockSize, blockSize);
      /*fill(0, 0, 0);
      stroke(0, 0, 0);
      text(this.name, x*blockSize+blockSize/4, y*blockSize+blockSize/2);*/
    }
    
    // Fire draw
    for(int i = 0; i < this.particles.size(); i++) {
      this.particles.get(i).draw();
    }
    
  }
  public boolean collide() {
    return this.collide;
  }
  public String getName() {
    return this.name;
  }
  public void fire() {
    this.onFire = true;
  }
  public void water() {
    this.onFire = false;
    this.fireTick = 0;
  }
}
class Pillar extends Block {
  private String name = "Pillar";
  private int x;
  private int y;
  private boolean fancy = true;
  
  public Pillar(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public boolean update() {
    return true;
  }
  public void draw() {
    rectMode(CORNER);
    resetMatrix();
    
    if(this.fancy) {
      fill(75, 75, 75);
      stroke(25, 25, 25);
      rect(this.x*blockSize, this.y*blockSize, blockSize, blockSize);
      fill(100, 100, 100);
      rect(this.x*blockSize + blockSize/4, this.y*blockSize + blockSize/4, blockSize/2, blockSize/2);
    }
    else {
      fill(75, 75, 75);
      stroke(25, 25, 25);
      rect(this.x*blockSize, this.y*blockSize, blockSize, blockSize);
      /*fill(0, 0, 0);
      stroke(0, 0, 0);
      text(this.name, this.x*blockSize+blockSize/4, this.y*blockSize+blockSize/2);*/
    }
  }
  public boolean collide() {
    return true;
  }
  public String getName() {
    return this.name;
  }
  public void fire() {};
  public void water() {};
}

// ######################################################## WAND ########################################################

abstract class Wand {
  abstract public void reset();
  abstract public void update();
  abstract public void draw(PVector posI);
  abstract public void drawSpells();
  abstract public void spell(PVector posI, PVector trajI);
  
  // The position of the extremity of the wand
  PVector wandPos(PVector playerPos, int playerSize, int wandSize) {
    float angle = atan2(mouseY - (playerPos.y + playerSize/2), mouseX - (playerPos.x + playerSize/2));
    return new PVector(playerPos.x + playerSize/2 + (playerSize/2 + wandSize) * cos(angle) + playerSize/2 * sin(angle+PI)  , playerPos.y + playerSize/2 + (playerSize/2 + wandSize) * sin(angle) - playerSize/2 * cos(angle+PI));
  }
  
  // The angle of shoot from the wand extremity position
  float wandAngle(PVector wandPos) {
    return atan2(mouseY - wandPos.y, mouseX - wandPos.x);
  }
  
  // The position of impact of the wand shooting int the mouse direction
  PVector wandImpact(PVector playerPos, int playerSize, int wandSize) {
    PVector wandPos = this.wandPos(playerPos, playerSize, wandSize);
    return Collision.getCollision(wandPos, PVector.fromAngle(wandAngle(wandPos)));
  }
  
  // The position of impact of the wand shooting straight
  PVector wandStraightImpact(PVector playerPos, int playerSize, int wandSize) {
    PVector wandPos = this.wandPos(playerPos, playerSize, wandSize);
    return Collision.getCollision(wandPos, PVector.fromAngle(middleAngle(this.middlePos(playerPos, playerSize))));
  }
  
  // The PVector trajectory of the wand angle
  PVector wandTraj(PVector playerPos, int playerSize, int wandSize) {
    PVector wandPos = this.wandPos(playerPos, playerSize, wandSize);
    return PVector.fromAngle(this.wandAngle(wandPos));
  }
  
  // The position of the middle front of the player
  PVector middlePos(PVector playerPos, int playerSize) {
    float angle = atan2(mouseY - (playerPos.y + playerSize/2), mouseX - (playerPos.x + playerSize/2));
    return new PVector(playerPos.x + playerSize/2 * cos(angle) + playerSize/2, playerPos.y + playerSize/2 * sin(angle) + playerSize/2);
  }
  
  // The angle the player is looking at
  float middleAngle(PVector middlePos) {
    return atan2(mouseY - middlePos.y, mouseX - middlePos.x);
  }
  
  // The position of impact where the player is looking at
  PVector middleImpact(PVector playerPos, int playerSize) {
    PVector middlePos = this.middlePos(playerPos, playerSize);
    return Collision.getCollision(middlePos, PVector.fromAngle(middleAngle(middlePos)));
  }
  
  // The PVector trajectory of the player angle
  PVector middleTraj(PVector playerPos, int playerSize) {
    PVector middlePos = this.middlePos(playerPos, playerSize);
    return PVector.fromAngle(this.middleAngle(middlePos));
  }
}

class LaserWand extends Wand {
  private int playerSize;
  private int size;
  private int cooldown = 0;
  private int time = 0;

  public LaserWand(int playerSize) {
    this.playerSize = playerSize;
    this.size = playerSize/2;
  }
  
  public void reset() {
  }
  
  public void update() {
    // Cooldown
    if(this.time > 0) {
      this.time--;
    }
  }

  public void draw(PVector posI) {
    // Draw wand
    rectMode(CENTER);
    //translate(this.x, this.y);

    stroke(0, 0, 0);
    line(this.playerSize/2, -this.playerSize/2, this.playerSize/2, -this.playerSize/2 - this.size);
    rectMode(CORNER);
  }
  
  public void drawSpells() {}

  public void spell(PVector posI, PVector trajI) {
    // Draw lasers
    if(this.time == 0) {
      this.time = this.cooldown;
      PVector playerPos = posI.copy();
      
      
      
      // Blue using the player angle and the red length (bad)
      /*PVector col = Collision.getCollision(playerPos, traj);
      stroke(0, 0, 255);
      //line(this.playerSize/2, -this.playerSize/2 - this.playerSize/2, this.playerSize/2, -this.playerSize/2 - this.playerSize*10);
      line(this.playerSize/2, -this.playerSize/2 - this.size, this.playerSize/2, -this.playerSize/2 - playerPos.dist(col));
      */
      
      resetMatrix();
      rectMode(CORNER);
      
      // Red coming from the corner pos of the player
      /*PVector traj = trajI.copy();
      stroke(255, 0, 0);
      PVector col2 = Collision.getCollision(playerPos, traj);
      line(playerPos.x, playerPos.y, col2.x, col2.y);
      */
      
      // Green coming from the middle front of the player
      /*stroke(0, 255, 0);
      float angle = atan2(mouseY - (playerPos.y + this.playerSize/2), mouseX - (playerPos.x + this.playerSize/2));
      PVector nez = new PVector(playerPos.x + this.playerSize/2 * cos(angle) + playerSize/2, playerPos.y + this.playerSize/2 * sin(angle) + this.playerSize/2);
      float angleNez = atan2(mouseY - nez.y, mouseX - nez.x);
      PVector colNez = Collision.getCollision(nez, PVector.fromAngle(angleNez));
      line(nez.x, nez.y, colNez.x, colNez.y);
      */
      
      
      
      // Light blue test coming from the wand
      /*stroke(0, 255, 255);
      float angle = atan2(mouseY - (playerPos.y + this.playerSize/2), mouseX - (playerPos.x + this.playerSize/2));
      PVector baguette = new PVector(playerPos.x + this.playerSize/2 + (this.playerSize/2 + this.size) * cos(angle) + this.playerSize/2 * sin(angle+PI)  , playerPos.y + this.playerSize/2 + (this.playerSize/2 + this.size) * sin(angle) - this.playerSize/2 * cos(angle+PI));
      float angleBaguette = atan2(mouseY - baguette.y, mouseX - baguette.x);
      PVector colBaguette = Collision.getCollision(baguette, PVector.fromAngle(angleBaguette));
      line(baguette.x, baguette.y, colBaguette.x, colBaguette.y);
      text(playerPos.x, 100, 150);
      text(playerPos.y, 100, 160);
      text(colBaguette.x, 100, 170);
      text(colBaguette.y, 100, 180);
      stroke(0, 0, 0);
      point(colBaguette.x, colBaguette.y);
      */
      
      PVector wandPos = this.wandPos(playerPos, playerSize, size);
      PVector wandImpact = this.wandImpact(playerPos, playerSize, size);
      stroke(255, 0, 0);
      line(wandPos.x, wandPos.y, wandImpact.x, wandImpact.y);
      
      PVector wandStraightImpact = this.wandStraightImpact(playerPos, playerSize, size);
      stroke(255, 255, 0);
      line(wandPos.x, wandPos.y, wandStraightImpact.x, wandStraightImpact.y);
      
      PVector middlePos = this.middlePos(playerPos, playerSize);
      PVector middleImpact = this.middleImpact(playerPos, playerSize);
      stroke(0, 0, 255);
      line(middlePos.x, middlePos.y, middleImpact.x, middleImpact.y);
      
      //line(playerPos.x + this.playerSize/2 + (this.playerSize/2 + this.size) * cos(angle) + this.playerSize/2 * sin(angle+PI)  , playerPos.y + this.playerSize/2 + (this.playerSize/2 + this.size) * sin(angle) - this.playerSize/2 * cos(angle+PI), 0, 0);
      
    }
    
  }
}

class BallWand extends Wand {
  private int playerSize;
  private int size;
  private int cooldown = 50;
  private int time = 0;
  private ArrayList<Particle> particles = new ArrayList<Particle>();

  public BallWand(int playerSize) {
    this.playerSize = playerSize;
    this.size = playerSize/2;
  }
  
  public void reset() {
    this.particles.clear();
  }
  
  public void update() {
    // Cooldown
    if(this.time > 0) {
      this.time--;
    }
    // Move and delete particles
    for(int i = 0; i < this.particles.size(); i++) {
      if(!this.particles.get(i).update()) {
        this.particles.remove(i);
      }
    }
  }

  public void draw(PVector posI) {
    // Draw wand
    rectMode(CENTER);
    //translate(this.x, this.y);

    stroke(255, 255, 0);
    line(this.playerSize/2, -this.playerSize/2, this.playerSize/2, -this.playerSize/2 - this.size);
    rectMode(CORNER);
    resetMatrix();
    
    
    // LaserPointer
    PVector playerPos = posI.copy();
    PVector wandImpact = wandImpact(playerPos, playerSize, size);
    stroke(255, 255, 255);
    circle(wandImpact.x, wandImpact.y, 1);
    
    
  }
  
  public void drawSpells() {
    // Draw balls
    for(int i = 0; i < this.particles.size(); i++) {
        this.particles.get(i).draw();
    }
  }

  public void spell(PVector posI, PVector trajI) {
    // Create ball
    if(this.time == 0) {
      this.time = this.cooldown;
      
      PVector playerPos = posI.copy();
      //PVector traj = trajI.copy();
      
      resetMatrix();
      rectMode(CORNER);
      
      PVector wandPos = this.wandPos(playerPos, playerSize, size);
      PVector wandImpact = this.wandImpact(playerPos, playerSize, size);
      PVector wandTraj = this.wandTraj(playerPos, playerSize, size);
      
      this.particles.add(new BallExplosion(wandPos, wandTraj, 3, 1));
      //this.particles.add(new Ball(wandPos, wandTraj, 3, 1));
      //this.particles.add(new BouncingBall(wandPos, wandTraj, wandImpact, 3, 1, 3));
    }
  }
}

class FireWand extends Wand {
  private int playerSize;
  private int size;
  private int cooldown = 20;
  private int time = 0;
  private ArrayList<Particle> particles = new ArrayList<Particle>();
  private int intensity = 5;

  public FireWand(int playerSize) {
    this.playerSize = playerSize;
    this.size = playerSize/2;
  }
  
  public void reset() {
    this.particles.clear();
  }
  
  public void update() {
    // Cooldown
    if(this.time > 0) {
      this.time--;
    }
    // Move and delete fire particles
    for(int i = 0; i < this.particles.size(); i++) {
      if(!this.particles.get(i).update()) {
        this.particles.remove(i);
      }
    }
  }

  public void draw(PVector posI) {
    // Draw wand
    rectMode(CENTER);
    //translate(this.x, this.y);

    stroke(255, 0, 0);
    line(this.playerSize/2, -this.playerSize/2, this.playerSize/2, -this.playerSize/2 - this.size);
    rectMode(CORNER);
    resetMatrix();
    
    // Fire effect of the wand
    PVector playerPos = posI.copy();
    PVector wandPos = this.wandPos(playerPos, playerSize, size);
    for(int i = 1; i <= this.intensity; i++) {
      this.particles.add(new Fire(wandPos, new PVector(0, 0), 50, 0.15, 0.25));
    }
    
    // Laser Pointer
    PVector wandImpact = wandImpact(playerPos, playerSize, size);
    stroke(255, 255, 255);
    circle(wandImpact.x, wandImpact.y, 1);
  }
  
  public void drawSpells() {
    // Draw fireball
    for(int i = 0; i < this.particles.size(); i++) {
        this.particles.get(i).draw();
    }
  }

  public void spell(PVector posI, PVector trajI) {
    // Create fireball
    if(this.time == 0) {
      this.time = this.cooldown;
      
      PVector playerPos = posI.copy();
      PVector wandPos = this.wandPos(playerPos, playerSize, size);
      PVector wandTraj = this.wandTraj(playerPos, playerSize, size);
      this.particles.add(new FireBall(wandPos, wandTraj, 4, this.intensity));
    }
    
  }
}

class WaterWand extends Wand {
  private int playerSize;
  private int size;
  private int cooldown = 20;
  private int time = 0;
  private ArrayList<Particle> particles = new ArrayList<Particle>();
  private int intensity = 5;
  private int waterSize = 5;

  public WaterWand(int playerSize) {
    this.playerSize = playerSize;
    this.size = playerSize/2;
  }
  
  public void reset() {
    this.particles.clear();
  }
  
  public void update() {
    // Cooldown
    if(this.time > 0) {
      this.time--;
    }
    // Move and delete fire particles
    for(int i = 0; i < this.particles.size(); i++) {
      if(!this.particles.get(i).update()) {
        this.particles.remove(i);
      }
    }
  }

  public void draw(PVector posI) {
    // Draw wand
    rectMode(CENTER);
    //translate(this.x, this.y);

    stroke(255, 0, 0);
    line(this.playerSize/2, -this.playerSize/2, this.playerSize/2, -this.playerSize/2 - this.size);
    rectMode(CORNER);
    resetMatrix();
    
    // Fire effect of the wand
    PVector playerPos = posI.copy();
    PVector wandPos = this.wandPos(playerPos, playerSize, size);
    for(int i = 1; i <= this.intensity; i++) {
      this.particles.add(new Water(wandPos, new PVector(0, 0), 50, 0.15, 0.25, this.waterSize));
    }
    
    // Laser Pointer
    PVector wandImpact = wandImpact(playerPos, playerSize, size);
    stroke(255, 255, 255);
    circle(wandImpact.x, wandImpact.y, 1);
  }
  
  public void drawSpells() {
    // Draw fireball
    for(int i = 0; i < this.particles.size(); i++) {
        this.particles.get(i).draw();
    }
  }

  public void spell(PVector posI, PVector trajI) {
    // Create fireball
    if(this.time == 0) {
      this.time = this.cooldown;
      
      PVector playerPos = posI.copy();
      PVector wandPos = this.wandPos(playerPos, playerSize, size);
      PVector wandTraj = this.wandTraj(playerPos, playerSize, size);
      this.particles.add(new WaterBall(wandPos, wandTraj, 4, this.intensity, this.waterSize));
    }
    
  }
}

// ######################################################## WORLD ########################################################

class World {
  public Block[][] map;
  public Player player;

  public World(int mapX, int mapY) {
   this. map = new Block[mapY][mapX];

    // Create an empty map (full of grass)
    for (int y = 0; y < mapY; y++) {
      for (int x = 0; x < mapX; x++) {
        this.map[y][x] = new Grass(x, y);
      }
    }

    // Generate map walls (important to stop projectiles from leaving the map)
    for (int y = 0; y < mapY; y++) {
      for (int x = 0; x < mapX; x++) {
        if (x==0 || y==0 || x==mapX-1 || y==mapY-1) {
          this.map[y][x] = new Wall(x, y);
        }
      }
    }

    // Random objects test
    /*for (int y = 1; y < mapY-1; y++) {
      for (int x = 1; x < mapX-1; x++) {
        if (random(100) < 5) {
          map[y][x] = new Pillar(x, y);
        }
      }
    }*/
    
    this.createSpawn();
    this.createWoodHouse(8, 5, 1000);
    this.createWoodHouse(4, 4, 1000);
    this.createWoodHouse(5, 5, 1000);
    this.createWoodHouse(4, 6, 1000);
    this.createWoodHouse(5, 7, 1000);
    this.createWoodHouse(4, 4, 1000);
    this.createWoodHouse(5, 4, 1000);
    this.createWoodHouse(6, 4, 1000);
    this.createWoodHouse(7, 6, 1000);
    this.createPillarBlock(1, 2, 1000);
    this.createPillarBlock(2, 1, 1000);
    this.createPillarBlock(2, 2, 1000);
    this.createPillarBlock(3, 3, 1000);
    
  }
  
  private void createSpawn() {
    int size = 3;
    int x;
    int y;
    // 1 to mapSize-1+1 to generates int between 1 and mapSize-1
    x = (int)random(1, mapX-size);
    y = (int)random(1, mapY-size);
    
    // Places a 3*3 steps at player spawn
    for(int spawnY = y; spawnY <= y+size-1; spawnY++) {
      for(int spawnX = x; spawnX <= x+size-1; spawnX++) {
        this.map[spawnY][spawnX] = new Steps(spawnX, spawnY);
      }
    }
    
    // Places the player at the spawn
    this.player = new Player(x+1, y+1);
  }
  
  private boolean createWoodHouse(int sizeX, int sizeY, int attempt) {

    // Making houses 3 wide minimum
    if(sizeX < 3) {
      System.out.println("Une maison de largeur " + sizeX + " est trop petite!");
      sizeX = 3;
    }
    if(sizeY < 3) {
      System.out.println("Une maison de hauteur " + sizeY + " est trop petite!");
      sizeY = 3;
    }
    
    // Free house spot finder
    int test = 0;
    int x = (int)random(1, mapX-sizeX-1);
    int y = (int)random(1, mapY-sizeY-1);
    while(!this.freeSpot(x-1, y-1, sizeX+2, sizeY+2) && test < attempt) {
      x = (int)random(1, mapX-sizeX-1);
      y = (int)random(1, mapY-sizeY-1);
      
      test++;
    }
    
    // Generate a house if a free spot was found
    if(this.freeSpot(x-1, y-1, sizeX+2, sizeY+2)) {
      
      // Generate a house (Walls and Floor)
      for(int posY = y; posY <= y+sizeY-1; posY++) {
        for(int posX = x; posX <= x+sizeX-1; posX++) {
          if (posX==x || posY==y || posX==x+sizeX-1 || posY==y+sizeY-1) {
            this.map[posY][posX] = new Wood(posX, posY);
          }
          else {
            this.map[posY][posX] = new Flooring(posX, posY);
          }
        }
      }
      
      // Door generation with Steps pathfinder
      int tempX;
      int tempY;
      ArrayList<PVector> path = new ArrayList<PVector>();
      switch((int)random(1, 5)) { // 1 to 5 to generates int between 1 and 4
        case 1:
          //haut
          tempX = (int)random(x+1, x+sizeX-1);
          this.map[y][tempX] = new Flooring(tempX, y);
          path = BlockFinder.findBlock(this.map, tempX, y-1, "Steps");
          break;
        case 2:
          // bas
          tempX = (int)random(x+1, x+sizeX-1);
          this.map[y+sizeY-1][tempX] = new Flooring(tempX, y+sizeY-1);
          path = BlockFinder.findBlock(this.map, tempX, y+sizeY, "Steps");
          break;
        case 3:
          //gauche
          tempY = (int)random(y+1, y+sizeY-1);
          this.map[tempY][x] = new Flooring(x, tempY);
          path = BlockFinder.findBlock(this.map, x-1, tempY, "Steps");
          break;
        case 4:
          // droite
          tempY = (int)random(y+1, y+sizeY-1);
          this.map[tempY][x+sizeX-1] = new Flooring(x+sizeX-1, tempY);
          path = BlockFinder.findBlock(this.map, x+sizeX, tempY, "Steps");
          break;
          
      }

      // Steps placer using the pathfinder path
      for(PVector p : path) {
        this.map[(int)p.y][(int)p.x] = new Steps((int)p.x, (int)p.y);
      }
      
      return true;
    }
    else {
      System.out.println("no spot found wood house");
      return false;
    }
  }
  
  private boolean createPillarBlock(int sizeX, int sizeY, int attempt) {
    // Free Stone spot finder
    int test = 0;
    int x = (int)random(1, mapX-sizeX-1);
    int y = (int)random(1, mapY-sizeY-1);
    while(!this.freeSpot(x-1, y-1, sizeX+2, sizeY+2) && test < attempt) {
      x = (int)random(1, mapX-sizeX-1);
      y = (int)random(1, mapY-sizeY-1);
      
      test++;
    }
    
    // Generate a house if a free spot was found
    if(this.freeSpot(x-1, y-1, sizeX+2, sizeY+2)) {
      
      // Generate a house (Walls and Floor)
      for(int posY = y; posY <= y+sizeY-1; posY++) {
        for(int posX = x; posX <= x+sizeX-1; posX++) {
            this.map[posY][posX] = new Pillar(posX, posY);
        }
      }
      return true;
    }
    else {
      System.out.println("no spot found wood house");
      return false;
    }
  }
  
  private boolean freeSpot(int x, int y, int sizeX, int sizeY) {
    // Test if a spot has a sizeX*sizeY free area
    for(int posY = y; posY <= y+sizeY; posY++) {
      for(int posX = x; posX <= x+sizeX; posX++) {
        if(this.map[posY][posX].getName() != "Grass") {
          //System.out.println(posX + " " + posY + " " + this.map[posY][posX].getName());
          return false;
        }
      }
    }
    return true;
  }
  
  public void update() {
    // Update the player and dependence
    this.player.update();
  }

  public void draw() {
    // Draw the map
    for (int y = 0; y < mapY; y++) {
      for (int x = 0; x < mapX; x++) {
        if(!this.map[y][x].update()) {
          this.map[y][x] = new Grass(x, y);
        }
        this.map[y][x].draw();
      }
    }
    // Draw the player and dependence
    this.player.draw();
  }
}

// ######################################################## PLAYER ########################################################

class Player {
  //private int x;
  //private int y;
  private PVector pos;
  private int playerSize;
  private PVector v;
  private ArrayList<Wand> wands = new ArrayList<Wand>();
  private int selectedWand;
  private int speed = 2;

  public Player(int x, int y) {
    //this.x = (int)random(width);
    //this.y = (int)random(height);
    //this.x = 100;
    //this.y = 100;
    this.pos = new PVector(x*blockSize, y*blockSize);
    this.playerSize = blockSize;
    this.selectedWand = 2;
    this.wands.add(new LaserWand(this.playerSize));
    this.wands.add(new BallWand(this.playerSize));
    this.wands.add(new FireWand(this.playerSize));
    this.wands.add(new WaterWand(this.playerSize));
  }
  
  public void selectWand(int nb) {
    if(nb < this.wands.size()) {
      //this.wands.get(this.selectedWand).reset();
      this.selectedWand = nb;
    }
  }
  
  public void spell() {
    // Cast the selected wand spells
    this.v = PVector.fromAngle(this.lookingAt());
    this.wands.get(this.selectedWand).spell(this.pos, this.v);
  }

  public float lookingAt() {
    return atan2(mouseY - (this.pos.y + this.playerSize/2), mouseX - (this.pos.x + this.playerSize/2));
    
  }
  
  public void update() {
    this.move();
    // update wands
    for(Wand w : this.wands) {
      w.update();
    }
  }

  public void draw() {
    rectMode(CENTER);
    translate(this.pos.x+playerSize/2, this.pos.y+playerSize/2);
    rotate(this.lookingAt() + HALF_PI);

    noStroke();
    fill(255, 217, 179);
    // rect(0, 0, this.playerSize, this.playerSize); //corps original
    rect(0, 0, this.playerSize, this.playerSize*0.9);
    fill(139, 69, 19);
    rect(-this.playerSize*0.45, -this.playerSize*0.45, this.playerSize*0.1, this.playerSize*0.1);
    rect(this.playerSize*0.45, -this.playerSize*0.45, this.playerSize*0.1, this.playerSize*0.1);
    fill(153, 0, 153);
    circle(0, 0, this.playerSize*0.8);
    fill(204, 0, 204);
    circle(0, 0, this.playerSize*0.5);
    
    
    
    this.wands.get(this.selectedWand).draw(this.pos);
    
    for(Wand w : this.wands) {
      w.drawSpells();
    }
    
    if (mousePressed) this.spell();
    rectMode(CORNER);
    resetMatrix();
    
    point(this.pos.x + this.playerSize/2 * cos(this.lookingAt()) + playerSize/2, this.pos.y + this.playerSize/2 * sin(this.lookingAt()) + playerSize/2);
    
    
    
    /*this.v = PVector.fromAngle(this.angle);
    text(this.v.x,100, 100);
    text(this.v.y,100, 110);*/
  }

  public void move() {
    if (upPressed) this.goUp();
    if (downPressed) this.goDown();
    if (leftPressed) this.goLeft();
    if (rightPressed) this.goRight();
  }

  public void goUp() {
    for(int i = 1; i <= this.speed; i++) {
      if (this.canGoUp()) {
        this.pos.y--;
      }
    }
  }
  public boolean canGoUp() {
    for (int i = (int)this.pos.x/blockSize; i <= (int)(this.pos.x+playerSize-1)/blockSize; i++) {
      if (world.map[(int)(this.pos.y-1)/blockSize][i].collide()) {
        return false;
      }
    }
    return true;
  }

  public void goDown() {
    for(int i = 1; i <= this.speed; i++) {
      if (this.canGoDown()) {
        this.pos.y++;
      }
    }
  }
  public boolean canGoDown() {
    for (int i = (int)this.pos.x/blockSize; i <= (int)(this.pos.x+playerSize-1)/blockSize; i++) {
      if (world.map[(int)(this.pos.y+this.playerSize)/blockSize][i].collide()) {
        return false;
      }
    }
    return true;
  }

  public void goLeft() {
    for(int i = 1; i <= this.speed; i++) {
      if (this.canGoLeft()) {
        this.pos.x--;
      }
    }
  }
  public boolean canGoLeft() {
    for (int i = (int)this.pos.y/blockSize; i <= (int)(this.pos.y+playerSize-1)/blockSize; i++) {
      if (world.map[i][(int)(this.pos.x-1)/blockSize].collide()) {
        return false;
      }
    }
    return true;
  }

  public void goRight() {
    for(int i = 1; i <= this.speed; i++) {
      if (this.canGoRight()) {
        this.pos.x++;
      }
    }
  }
  public boolean canGoRight() {
    for (int i = (int)this.pos.y/blockSize; i <= (int)(this.pos.y+playerSize-1)/blockSize; i++) {
      if (world.map[i][(int)(this.pos.x+this.playerSize)/blockSize].collide()) {
        return false;
      }
    }
    return true;
  }
}

// ######################################################## COLLISION ########################################################

public static class Collision {
  public static PVector getCollision(PVector posI, PVector trajI) {
    // Get the spot where the given trajectory will collide
    PVector pos = posI.copy();
    PVector traj = trajI.copy();
    
    //int blockX = (int)(pos.x+traj.x)/blockSize;
    //int blockY = (int)(pos.y+traj.y)/blockSize;
    while(!world.map[(int)(pos.y+traj.y)/blockSize][(int)(pos.x+traj.x)/blockSize].collide()) {
      pos.add(traj);
    }
    
    return pos;
  }
}

// ######################################################## PARTICLE ########################################################

abstract class Particle {
  abstract boolean update();
  abstract void draw();
}

public class Ball extends Particle {
  private PVector pos;
  private PVector traj;
  private int speed;
  private int size;
  
  
  public Ball(PVector posI, PVector trajI, int speed, int size) {
    this.pos = posI.copy();
    this.traj = trajI.copy();
    this.speed = speed;
    this.size = size;
  }
  
  public boolean update() {
    // Update the Ball
    for(int s = 1; s <= this.speed; s++ ) {
      if(!world.map[(int)(this.pos.y+this.traj.y)/blockSize][(int)(this.pos.x+this.traj.x)/blockSize].collide()){
        this.pos.add(traj);      
      }
      else {
        return false;
      }
    }
    return true;
  }
  
  public void draw() {
    // Draw the ball
    resetMatrix();
    rectMode(CORNER);
    stroke(255, 255, 0);
    fill(255, 255, 0);
    circle(this.pos.x, this.pos.y, this.size);
  }
  
}

public class BallExplosion extends Particle {
  private PVector pos;
  private PVector traj;
  private int speed;
  private int size;
  private boolean collided = false;
  private ArrayList<Particle> particles = new ArrayList<Particle>();
  
  
  public BallExplosion(PVector posI, PVector trajI, int speed, int size) {
    this.pos = posI.copy();
    this.traj = trajI.copy();
    this.speed = speed;
    this.size = size;
  }
  
  public boolean update() {
    // Update the ball and dependence
    if(!this.collided)  {
      // Move BallExplosion
      for(int s = 1; s <= this.speed; s++ ) {
        if(!world.map[(int)(this.pos.y+this.traj.y)/blockSize][(int)(this.pos.x+this.traj.x)/blockSize].collide()){
          this.pos.add(traj);      
        }
        else {
          this.collided = true;
          for(int i = 1; i <= 360; i++) {
            this.particles.add(new Ball(this.pos, PVector.fromAngle(i), this.speed, this.size));
            this.particles.add(new BouncingBall(this.pos, PVector.fromAngle(i), Collision.getCollision(this.pos, PVector.fromAngle(i)), this.speed, this.size, 3));
          }
        }
      }
      return true;
    }
    else {
      // Move and update Balls
      if(this.particles.size() > 0) {
        for(int i = 0; i < this.particles.size(); i++) {
          if(!this.particles.get(i).update()) {
            this.particles.remove(i);
          }
        }
        return true;
      }
      else {
        return false;
      }
    }
  }
  
  public void draw() {
    // Draw the ball and dependence
    if(!this.collided) {
      resetMatrix();
      rectMode(CORNER);
      stroke(255, 255, 0);
      fill(255, 255, 0);
      circle(this.pos.x, this.pos.y, this.size);
    }
    for(int i = 0; i < this.particles.size(); i++) {
      this.particles.get(i).draw();
    }
  }
}

public class BouncingBall extends Particle {
  private PVector pos;
  private PVector traj;
  private PVector col;
  private int speed;
  private int size;
  private int bounces;
  private float angle;
  
  public BouncingBall(PVector posI, PVector trajI, PVector col, int speed, int size, int bounces) {
    this.pos = posI.copy();
    this.traj = trajI.copy();
    this.col = col.copy();
    this.speed = speed;
    this.size = size;
    this.bounces = bounces;
  }
  
  public boolean update() {
    // Update the ball
    
    // Update angle
    //float x = atan2(pos.y - col.y,  pos.x - col.x);
    this. angle = -(this.traj.heading()/PI * 180) % 360;
    if(this.angle < 0) {
        this.angle = angle + 360;
    }
    
    // Update collision point if map changes
    this.col = Collision.getCollision(this.pos, this.traj);
    
    // Move or bounce
    for(int s = 1; s <= this.speed; s++ ) {
      if(!this.pos.equals(this.col)/*world.map[(int)(this.pos.y+this.traj.y)/blockSize][(int)(this.pos.x+this.traj.x)/blockSize] == ' '*/) {
        this.pos.add(traj);
      }
      else {
        
        PVector blockCol = this.col.add(this.traj);
        int blockX = (int)(blockCol.x/blockSize);
        int blockY = (int)(blockCol.y/blockSize);
        
        if(this.pos.x <= blockX*blockSize ) {
          // left
          System.out.println("left " + angle + " " + (180 + 180 - angle));
          this.traj = PVector.fromAngle(180 + 180 - angle); 
        }
        else if(this.pos.y <= blockY*blockSize) {
          // up
          System.out.println("up " + angle + " " + (90 + 90 - angle));
          this.traj = PVector.fromAngle(90 + 90 - angle);
        }
        else if(this.pos.x >= blockX*blockSize + blockSize) {
          // right
          System.out.println("right " + angle + " " + (360 - angle));
          this.traj = PVector.fromAngle(360 - angle);
        }
        else if(this.pos.y >= blockY*blockSize + blockSize) {
          // bottom
          System.out.println("bottom " + angle + " " + (270 + 270 - angle));
          this.traj = PVector.fromAngle(270 + 270 - angle); 
        }
        
        //System.out.println(angle);
        // TOUT EST MAUVAIS QUELQUE CHOSE QUI VA PLUS VERS LA DROITE PEUT COGNER LE DESSUS D'UNE CHOSE
        // Incorrect because something going to the right do not necessiraly  bounce on a right wall
        /*if ( (angle >= 0 && angle <= 45)) {
          // right top
          this.traj = PVector.fromAngle(360 - angle);
        }
        else if(angle >= 270 && angle <= 360){
          // right bottom
          this.traj = PVector.fromAngle(360 - angle);
        }
        else if (angle >= 45 && angle <= 135) {
          // up
          this.traj = PVector.fromAngle(90 + 90 - angle); 
        }
        else if (angle >= 135 && angle <= 225) {
          // left
          this.traj = PVector.fromAngle(180 + 180 - angle); 
        }
        else if (angle >= 135 && angle <= 235) {
          // bottom
          this.traj = PVector.fromAngle(270 + 270 - angle); 
        }
        else {
          //System.out.println(this.traj.heading());
          //this.traj = new PVector(-this.traj.x, -this.traj.y);
        }*/
        //this.traj.rotate(90);
        
        this.col = Collision.getCollision(this.pos, this.traj);
        this.bounces--;
      }
    }
    return this.bounces > 0;
  }
  
  public void draw() {
    // Draw the ball
    resetMatrix();
    rectMode(CORNER);
    fill(255, 255, 0);
    stroke(255, 255, 0);
    circle(this.pos.x, this.pos.y, this.size);
    
    fill(0, 0, 255);
    stroke(0, 0, 255);
    //circle(this.col.x, this.col.y, this.size);
    
    fill(0, 0, 0);
    stroke(0, 0, 0);
    //text(this.angle , this.pos.x, this.pos.y);
    //text(this.bounces , this.pos.x, this.pos.y + 10);
  }
  
  void drawParticles() {}
  
}

public class Fire extends Particle {
  private PVector pos;
  private PVector traj;
  private int life;
  private int time;
  private float dispersion;
  private float rise;
  
  public Fire(PVector posI, PVector trajI, int life, float dispersion, float rise) {
    this.pos = posI.copy();
    this.life = (int)random(life);
    this.dispersion = dispersion;
    this.traj = trajI.copy().add(new PVector(random(-this.dispersion, this.dispersion), random(-this.dispersion, this.dispersion)));;
    this.rise = rise;
    this.time = 0;
  }
  
  public boolean update() {
    // Update the fire particle
    this.time++;
    this.pos.add(traj);
    this.pos.add(new PVector(0, -this.rise));
    return this.time < this.life;
  }
  
  public void draw() {
    // Draw the fire particle
    resetMatrix();
    rectMode(CORNER);
    colorMode(HSB, 100);
    stroke(14 - 14*((float)this.time/(float)this.life), 100, 100);
    point(this.pos.x, this.pos.y);
    
    colorMode(RGB, 255);
  }
  
}

public class FireBall extends Particle {
  private PVector pos;
  private PVector traj;
  private int speed;
  private int intensity;
  private ArrayList<Particle> particles = new ArrayList<Particle>();
  
  
  public FireBall(PVector posI, PVector trajI, int speed, int intensity) {
    this.pos = posI.copy();
    this.traj = trajI.copy();
    this.speed = speed;
    this.intensity = intensity;
  }
  
  public boolean update() {
    // Update the fireball and dependence
    
    // Fireball movement
    for(int s = 1; s <= this.speed; s++ ) {
      if(!world.map[(int)(this.pos.y+this.traj.y)/blockSize][(int)(this.pos.x+this.traj.x)/blockSize].collide()){
        this.pos.add(traj);
        
        // Burn floor
        world.map[(int)(this.pos.y/blockSize)][(int)(this.pos.x/blockSize)].fire();
        
        // Fire effect
        for(int i = 1; i <= this.intensity; i++) {
          this.particles.add(new Fire(this.pos, new PVector(0, 0), 25, 0.15, 0.25));
        }
        
      }
      else {
        if(this.particles.size() <= 0) {
          return false;
        }
        // Burn wall
        world.map[(int)(this.pos.y+this.traj.y)/blockSize][(int)(this.pos.x+this.traj.x)/blockSize].fire();
      }
    }
    
    // Update fire particles
    for(int i = 0; i < this.particles.size(); i++) {
      if(!this.particles.get(i).update()) {
        this.particles.remove(i);
      }
    }
    
    return true;
  }
  
  public void draw() {
    // Draw fire particles
    for(int i = 0; i < this.particles.size(); i++) {
      this.particles.get(i).draw();
    }
  }
  
}

public class Water extends Particle {
  private PVector pos;
  private PVector traj;
  private int life;
  private int time;
  private float dispersion;
  private float fall;
  private int size;
  
  public Water(PVector posI, PVector trajI, int life, float dispersion, float fall, int size) {
    this.pos = posI.copy();
    this.life = (int)random(life);
    this.dispersion = dispersion;
    this.traj = trajI.copy().add(new PVector(random(-this.dispersion, this.dispersion), random(-this.dispersion, this.dispersion)));;
    this.fall = fall;
    this.size = size;
    this.time = 0;
  }
  
  public boolean update() {
    // Update the fire particle
    this.time++;
    this.pos.add(traj);
    this.pos.add(new PVector(0, this.fall));
    if(this.size > 1) {
      this.size--;
    }
    return this.time < this.life;
  }
  
  public void draw() {
    // Draw the fire particle
    resetMatrix();
    rectMode(CENTER);
    stroke(0, 0, 255);
    fill(0, 0, 255);
    circle(this.pos.x, this.pos.y, this.size);
    rectMode(CORNER);
  }
  
}

public class WaterBall extends Particle {
  private PVector pos;
  private PVector traj;
  private int speed;
  private int intensity;
  private int size;
  private ArrayList<Particle> particles = new ArrayList<Particle>();
  
  
  public WaterBall(PVector posI, PVector trajI, int speed, int intensity, int size) {
    this.pos = posI.copy();
    this.traj = trajI.copy();
    this.speed = speed;
    this.intensity = intensity;
    this.size = size;
  }
  
  public boolean update() {
    // Update the fireball and dependence
    
    // Waterball movement
    for(int s = 1; s <= this.speed; s++ ) {
      if(!world.map[(int)(this.pos.y+this.traj.y)/blockSize][(int)(this.pos.x+this.traj.x)/blockSize].collide()){
        this.pos.add(traj);
        
        // Water floor
        world.map[(int)(this.pos.y/blockSize)][(int)(this.pos.x/blockSize)].water();
        
        // Water effect
        for(int i = 1; i <= this.intensity; i++) {
          this.particles.add(new Water(this.pos, new PVector(0, 0), 25, 0.15, 0.25, this.size));
        }
        
      }
      else {
        if(this.particles.size() <= 0) {
          return false;
        }
        // Water wall
        world.map[(int)(this.pos.y+this.traj.y)/blockSize][(int)(this.pos.x+this.traj.x)/blockSize].water();
      }
    }
    
    // Update water particles
    for(int i = 0; i < this.particles.size(); i++) {
      if(!this.particles.get(i).update()) {
        this.particles.remove(i);
      }
    }
    
    return true;
  }
  
  public void draw() {
    // Draw fire particles
    for(int i = 0; i < this.particles.size(); i++) {
      this.particles.get(i).draw();
    }
  }
  
}

// ######################################################## BLOCKFINDER ########################################################

public static class BlockFinder {
  // PathFind to the nearest block given to find
  public static ArrayList<PVector> findBlock(Block[][] map, int x, int y, String b) {
    ArrayList<PVector> path  = new ArrayList<PVector>();
    BlockSearch[][] mapSearch = new BlockSearch[mapY][mapX];
    for (int posY = 0; posY < mapY; posY++) {
      for (int posX = 0; posX < mapX; posX++) {
        mapSearch[posY][posX] = null;
      }
    }
    BlockSearch source = new BlockSearch(x, y, b);
    mapSearch[y][x] = source;
    while(!source.update(map, mapSearch)) {}
    
    path.add(new PVector(x, y));
    BlockSearch way = source;
    while(way.getNext() != null) {
      path.add(new PVector(way.getNext().x, way.getNext().y));
      way = way.getNext();
    }
    
    System.out.println("Found path: ");
    for(PVector p : path) {
      System.out.println((int)p.x + " " +(int) p.y);
    }
    
    return path;
  }
}

public static class BlockSearch {
  private ArrayList<BlockSearch> next = new ArrayList<BlockSearch>();
  private int x;
  private int y;
  private String b;
  public boolean path = false;
  
  public BlockSearch(int x, int y, String b){
    //System.out.println("new BlockSearch " + x + " " + y);
    this.x = x;
    this.y = y;
    this.b = b;
  }
  
  public boolean update(Block[][] map, BlockSearch[][] mapSearch) {
    // Create others BlockSearch until we find the given block
    
    // can't use world.map so we transmit this.map
    if(map[this.y][this.x].getName() == b) {
      return true;
    }
    else if(next.size() == 0) {
      if(this.x-1 >= 0 && mapSearch[this.y][this.x-1] == null && !map[this.y][this.x-1].collide()) {
        BlockSearch leftBlock = new BlockSearch(this.x-1, this.y, this.b);
        this.next.add(leftBlock);
        mapSearch[this.y][this.x-1] = leftBlock;
        //text("left", (this.x-1) * blockSize, this.y * blockSize);
      }
      
      if(this.x+1 < mapX && mapSearch[this.y][this.x+1] == null && !map[this.y][this.x+1].collide()) {
        BlockSearch rightBlock = new BlockSearch(this.x+1, this.y, this.b);
        this.next.add(rightBlock);
        mapSearch[this.y][this.x+1] = rightBlock;
        //text("right", (this.x+1) * blockSize, this.y * blockSize);
      }
      
      if(this.y-1 >= 0 && mapSearch[this.y-1][this.x] == null && !map[this.y-1][this.x].collide()) {
        BlockSearch topBlock = new BlockSearch(this.x, this.y-1, this.b);
        this.next.add(topBlock);
        mapSearch[this.y-1][this.x] = topBlock;
        //text("top", this.x * blockSize, (this.y-1) * blockSize);
      }
      
      if(this.y+1 < mapY && mapSearch[this.y+1][this.x] == null && !map[this.y+1][this.x].collide()) {
        BlockSearch bottomBlock = new BlockSearch(this.x, this.y+1, this.b);
        this.next.add(bottomBlock);
        mapSearch[this.y+1][this.x] = bottomBlock;
        //text("bottom", this.x * blockSize, (this.y+1) * blockSize);
      }
    }
    else {
      for(BlockSearch n : this.next) {
        if(n.update(map, mapSearch)) {
          this.path = true;
          return true;
        }
      }
    }
    return false;
  }
  
  public BlockSearch getNext() {
    // Used to retrace the found path
    if(this.next.size() != 0) {
      for(BlockSearch s : this.next) {
        if(s.path) {
          return s;
        }
      }
    }
    return null;
  }
}

// ######################################################## INPUT ########################################################

final char up = 'z';
boolean upPressed = false;

final char down = 's';
boolean downPressed = false;

final char left = 'q';
boolean leftPressed = false;

final char right = 'd';
boolean rightPressed = false;


void keyPressed() {
  switch(key) {
    case up:
      upPressed = true;
      break;
    case down:
      downPressed = true;
      break;
    case left:
      leftPressed = true;
      break;
    case right:
      rightPressed = true;
      break;
    case '&':
    case '1':
      world.player.selectWand(0);
      break;
    case 'é':
    case '2':
      world.player.selectWand(1);
      break;
    case '"':
    case '3':
      world.player.selectWand(2);
      break;
    case '\'':
    case '4':
      world.player.selectWand(3);
      break;
    case '(':
    case '5':
      world.player.selectWand(3);
      break;
    case '-':
    case '6':
      world.player.selectWand(3);
      break;
    case 'è':
    case '7':
      world.player.selectWand(3);
      break;
    case '_':
    case '8':
      world.player.selectWand(3);
      break;
    case 'ç':
    case '9':
      world.player.selectWand(3);
      break;
    case 'à':
    case '0':
      world.player.selectWand(3);
      break;
  }
}

void keyReleased() {
  switch(key) {
  case up:
    upPressed = false;
    break;
  case down:
    downPressed = false;
    break;
  case left:
    leftPressed = false;
    break;
  case right:
    rightPressed = false;
    break;
  }
}
